﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHook : MonoBehaviour
{
    Animator anim;
    public AnimatorData data;

    public AnimAction[] animActions;

    private void OnAnimatorIK(int layerIndex)
    {
        for (int i = 0; i < animActions.Length; i++)
        {
            animActions[i].Execute(data);
        }
    }
}
