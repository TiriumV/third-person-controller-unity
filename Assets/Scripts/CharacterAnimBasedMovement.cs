﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
   
    [Header("Animator Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    private bool mirrorIdle;
    public string turn180Param = "turn180";
    private bool turn180;
    public bool walled;
    public bool falling;
    public bool sloping;

    [Header("Animator Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float degreesToTurn = 140f;
    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    public AnimatorData animdata;
    public float slopeForce;
    public float slopeForceRayLenght;

    private bool OnSlope(bool fall)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, characterController.height / 2 * slopeForceRayLenght))
        {
            if (hit.normal != Vector3.up)
            {
                sloping = true;
                return true;
            }
            sloping = true;
        }
        sloping = false;
        return false;
    }

    private void IsFalling()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, characterController.height * slopeForceRayLenght))
        {
            falling = false;
        }
        else
        {
            falling = true;
        }
    }
    private void Start()
    {
        
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        animdata = new AnimatorData(animator);
    }

    private void FixedUpdate()
    {
        IsFalling();
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        if (falling == true)
        {
            animator.SetBool("Falling", true);
        }
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if(Speed >= Speed - rotationThreshold && dash)
        {
            Speed = 2f;
        }

        if(Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;
            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turn180 = true;
            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                      Quaternion.LookRotation(desiredMoveDirection),
                                                      rotationSpeed * Time.deltaTime);
            }
            animator.SetBool(turn180Param, turn180);
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);

        }else if(Speed < rotationThreshold){
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }
        animator.SetBool("Wall", false);
        if (characterController.isGrounded)
        {
            if (walled)
            {
                animator.SetBool("Wall", true);
            }
            animator.SetBool("Falling", false);
            falling = false;
            if (jump)
            {
                if (Speed < 0.1f)
                {
                    animator.SetBool("StaticJump", true);
                }
                else
                {
                    Vector3 movevector = new Vector3(0f, 150f, 0f);
                    characterController.SimpleMove(movevector * Time.deltaTime);
                    animator.SetBool("RunJumping", true);
                    animator.SetBool("Wall", false);
                }    
            }
            else
            {
                animator.SetBool("RunJumping", false);
            }
        }
        if((vInput != 0 || hInput != 0) && OnSlope(falling)){
            characterController.Move(Vector3.down * characterController.height / 2 * slopeForce * Time.deltaTime);
        }
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToRightFoot > distanceToLeftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.transform.tag == "Walls")
        {
            walled = true;
        }
    }
}
