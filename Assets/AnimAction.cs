﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimAction : ScriptableObject
{
    public abstract void Execute(AnimatorData d);
}
