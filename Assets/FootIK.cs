﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Actions/AnimActions/Foot IK")]

public class FootIK : AnimAction 
{
    public AvatarIKGoal ikGoal;
    public string targetCurve;
    public float originOffset = 0.2f;
    public float feetOffset = 0.1f;

    public override void Execute(AnimatorData d)
    {
        Transform originTrans = d.rightFoot;
        if (ikGoal == AvatarIKGoal.LeftFoot)
        {
            originTrans = d.leftFoot;
        }
        
        float weight = d.anim.GetFloat(targetCurve);
        RaycastHit hit;
        Vector3 origin = originTrans.position;
        origin.y += originOffset;
        Vector3 dir = -Vector3.up;

        Vector3 targetPosition = origin;

        if (Physics.Raycast(origin, dir, out hit, 1f))
        {
            targetPosition = hit.point + (Vector3.up * feetOffset);
        }
        d.anim.SetIKPositionWeight(ikGoal, weight);
        d.anim.SetIKPosition(ikGoal, targetPosition);
    }

}
