﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorData
{
    public Transform leftFoot;
    public Transform rightFoot;
    public Animator anim;

    public AnimatorData(Animator anim)
    {
        this.anim = anim;
        leftFoot = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
        rightFoot = anim.GetBoneTransform(HumanBodyBones.RightFoot);

        AnimatorHook aHook = anim.GetComponent<AnimatorHook>();
        if (aHook == null) {
            aHook = anim.gameObject.AddComponent<AnimatorHook>();
        }
        aHook.data = this;
    }
}
